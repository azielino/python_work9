import sys
from definitions import actions, data_print, account, warehouse, history

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: magazyn.py result <str identyfikator produktu 1> <str identyfikator produktu 2> ... =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) > 1:
        print("=========== Błąd! ===========")
        print("===== Uruchom program w formie: magazyn.py result <str identyfikator produktu 1> <str identyfikator produktu 2> ... =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        history.append(input_list)
        if warehouse:
            with open('result.txt', "w") as file:
                for i in range (input_list.index(input_list[1]), len(input_list)):
                    file.write(f"{input_list[i]}: {warehouse[input_list[i]]}\n")
            with open('result.txt', "r") as file:
                print("\n" + file.read())
        else:
            print("====== Magazyn jest pusty ======")
else:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: magazyn.py result <str identyfikator produktu 1> <str identyfikator produktu 2> ... =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")