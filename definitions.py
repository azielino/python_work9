
ACTIONS = ("saldo", "zakup", "sprzedaz", "stop")
account = int()
warehouse = dict()
history = list()
history_str = list()

def num_check(num):
    while not (num.isnumeric()):
        num = input("! Podaj cene jednostkową w groszach: ")
    return int(num)

def history_print(h):
    print("\nWszystkie podane parametry w formie w jakiej zostały pobrane:")
    for list_in_history in h:
        for item in list_in_history:
            print(item)

def data_print(a, w, h):
    print("............................................")
    print(f"Stan konta: {a} gr\nMagazyn: {w}\nHistoria: {h}")

def actions(a, w, h):
    action = str()
    print("=========================== START ===========================")
    while not action == "stop":
        choice = input(f"Wpisz akcję z dozwolonych {ACTIONS}: ")
        if choice in ACTIONS:
            action = choice
        else:
            print("======== Błąd! ========\n====== Zła akcja ======\n===== STOP! =====")
            break
        if action == "saldo":
            account_pay = int(input("Podaj kwotę wpłaty w groszach (jak wypłata to podaj kwotę ujemną): "))
            a += account_pay
            comment = input("Dodaj komentarz: ")
            if not comment:
                comment = "- Nie dodano komentarza -"
            h.append([action, account_pay, comment])
        elif action == "zakup":
            while True:
                product = input("Podaj identyfikator kupowanego produktu: ")
                price = input("Podaj cene jednostkową w groszach: ")
                price = num_check(price)
                if price >= 0:                
                    product_quantity = input("Podaj liczbę sztuk: ")
                    while not (product_quantity.isnumeric() and product_quantity != "0"):
                        product_quantity = input("! Podaj liczbę sztuk: ")
                    product_quantity = int(product_quantity)
                    if product_quantity > 0:
                        a -= product_quantity*price
                        if a < 0:
                            a += product_quantity*price
                            print("======== Błąd! ========\n===== Za mało gotówki =====")
                            break
                        else:
                            if product in w:
                                w[product] += product_quantity
                            else:
                                w[product] = product_quantity
                    else:
                        print("======== Błąd! ========\n===== Zła ilość =====")
                        continue
                else:
                    print("======== Błąd! ========\n===== Ujemna cena =====")
                    continue
                break
            h.append([action, product, price, product_quantity])
        elif action == "sprzedaz":
            if w:
                while True:
                    print(f"Stan magazynu: {w}")
                    product = input("Podaj identyfikator sprzedawanego produktu: ")
                    if not product in w:
                        print("===== Błąd! =====\n===== Nie ma takiego towaru w magazynie =====")
                        continue
                    price = input("Podaj cene jednostkową w groszach: ")
                    price = num_check(price)
                    if price >= 0:
                        product_quantity = input("Podaj liczbę sprzedawanych sztuk: ")
                        while not (product_quantity.isnumeric() and product_quantity != "0"):
                            product_quantity = input("! Podaj liczbę sprzedawanych sztuk: ")
                        product_quantity = int(product_quantity)
                        if product_quantity <= w[product]:
                            a += product_quantity*price
                            w[product] -= product_quantity
                            if w[product] == 0:
                                del w[product]
                            h.append([action, product, price, product_quantity])
                            break
                        else:
                            print("======== Błąd! ========\n===== Zła ilość =====")
                            continue
                    else:
                        print("======== Błąd! ========\n===== Zła cena =====")
                        continue
            else:
                print("======== Błąd! ========\n===== Magazyn jest pusty =====")
        else:
            h.append([action])
    return a, w, h

def history_to_str(h):
    h_str = list()
    for list_in_history in h:
        for item in list_in_history:
            h_str.append(str(item))
    return h_str