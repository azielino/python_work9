import sys
from definitions import actions, history_to_str, data_print, account, warehouse, history, history_str

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: sprzedaz.py result <str identyfikator produktu> <int cena> <int liczba sprzedanych> =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) == 4:
        print("=========== Błąd! ===========")
        print("===== Uruchom program w formie: sprzedaz.py result <str identyfikator produktu> <int cena> <int liczba sprzedanych> =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        action = input_list[0]
        product = input_list[1]
        product_quantity = int(input_list[3])
        if warehouse[product] >= product_quantity:
            price = int(input_list[2])
            if price >= 0:
                if product_quantity > 0:
                    account += product_quantity*price
                    warehouse[product] -= product_quantity
                    if warehouse[product] == 0:
                            del warehouse[product]
                else:
                    print("======== Błąd! ========\n===== Zła ilość przy uruchomieniu programu =====")
            else:
                print("======== Błąd! ========\n===== Ujemna cena przy uruchomieniu programu =====")
        else:
            print("======== Błąd! ========\n===== Brak Towaru. Zła ilość przy uruchamianiu programu =====")
        history.append(input_list)
        history_str = history_to_str(history)
        with open('result.txt', "w") as file:
            for item in history_str:
                file.write(item + "\n")
        with open('result.txt', "r") as file:
            print("\n" + file.read())          
else:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: sprzedaz.py result <str identyfikator produktu> <int cena> <int liczba sprzedanych> =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")