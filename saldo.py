import sys
from definitions import actions, history_to_str, data_print, account, warehouse, history, history_str

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: saldo.py result <int wartosc> <str komentarz> =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) == 3:
        print("=========== Błąd! ===========")
        print("===== Uruchom program w formie: saldo.py result <int wartosc> <str komentarz> =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        action = input_list[0]
        account_pay = int(input_list[1])
        account += account_pay
        comment = input_list[2]
        history.append(input_list)
        history_str = history_to_str(history)
        with open('result.txt', "w") as file:
            for item in history_str:
                file.write(item + "\n")
        with open('result.txt', "r") as file:
            print("\n" + file.read())
else:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: saldo.py result <int wartosc> <str komentarz> =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")