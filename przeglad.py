import sys
from definitions import actions, data_print, history_to_str, account, warehouse, history

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: przeglad.py result <int zakres od> <int zakres do> =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) == 3:
        print("=========== Błąd! ===========")
        print("===== Uruchom program w formie: przeglad.py result <int zakres od> <int zakres do> =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        history.append(input_list)
        if not len(history) >= int(input_list[2]):
            print("===== Błąd! =====\n===== Podany zakres przekracza ilość zapisanych akcji ======")
        else:
            with open('result.txt', "w") as file:
                for list_in_history in range (int(input_list[1]), int(input_list[2]) + 1):
                    for list_item in history[list_in_history]:
                        file.write(str(list_item) + "\n")
            with open('result.txt', "r") as file:
                print("\n" + file.read())
else:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: przeglad.py result <int zakres od> <int zakres do> =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")