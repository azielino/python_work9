import sys
from definitions import actions, data_print, account, warehouse, history

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: konto.py result =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) == 1:
        print("=========== Błąd! ===========\n===== Uruchom program w formie: konto.py result =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        history.append(input_list)
        with open('result.txt', "w") as file:
            file.write(str(account))
        with open('result.txt', "r") as file:
            print("\n" + file.read())
else:
    print("=========== Błąd! ===========\n===== Uruchom program w formie: konto.py result =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")