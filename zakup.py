import sys
from definitions import actions, history_to_str, data_print, account, warehouse, history, history_str

input_list = sys.argv
if len(input_list) < 2:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: zakup.py result <str identyfikator produktu> <int cena> <int liczba zakupionych> =====")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = input_list[0].replace('.py', '')
    if not len(input_list) == 4:
        print("=========== Błąd! ===========")
        print("===== Uruchom program w formie: zakup.py result <str identyfikator produktu> <int cena> <int liczba zakupionych> =====")
    else:
        account, warehouse, history = actions(account, warehouse, history)
        action = input_list[0]
        product = input_list[1]
        price = int(input_list[2])
        if price >= 0:
            product_quantity = int(input_list[3])
            if product_quantity > 0:
                account -= product_quantity*price
                if account < 0:
                    account += product_quantity*price
                    print("======== Błąd! ========")
                    print("===== Brak gotówki na zakup towaru wpisanego przy uruchamianiu programu =====")
                else:
                    if product in warehouse:
                        warehouse[product] += product_quantity
                    else:
                        warehouse[product] = product_quantity
            else:
                print("======== Błąd! ========\n===== Zła ilość przy uruchamianiu programu =====")
        else:
            print("======== Błąd! ========\n===== Ujemna cena przy uruchamianiu programu =====")
        history.append(input_list)
        history_str = history_to_str(history)
        with open('result.txt', "w") as file:
            for item in history_str:
                file.write(item + "\n")
        with open('result.txt', "r") as file:
            print("\n" + file.read())
else:
    print("=========== Błąd! ===========")
    print("===== Uruchom program w formie: zakup.py result <str identyfikator produktu> <int cena> <int liczba zakupionych> =====")
data_print(account, warehouse, history)
print("=========================== STOP ===========================")